from importlib import import_module
import os
import sys

__CLI_ROOT__ = os.path.dirname(__file__)


class CliInterface(object):
    def __init__(self, root=None):
        self.root = root or os.path.dirname(sys.argv[0])
        self.__commands = {}
        self.__load_commands()

    def __load_commands(self):
        for ps in os.listdir(os.path.join(__CLI_ROOT__, 'commands')):
            if ps[0] == "_": continue
            cmd, _ = os.path.splitext(ps)
            mod = import_module('appcore.cli.commands.%s' % cmd)
            self.__commands[cmd] = getattr(mod, 'command')

    def run(self):
        argv = sys.argv[:]
        if len(argv) < 2:
            print "You didn't specify a command"
            self.list_commands()
            return

        cmd = argv[1]
        cmd_args = argv[2:]
        if not cmd in self.__commands:
            self.unknown_command()
            return

        args = self.__commands[argv[1]].parser.parse_args(cmd_args)
        self.__commands[argv[1]](**vars(args))

    def unknown_command(self):
        print "Unknown command."
        self.list_commands()

    def list_commands(self):
        print "The available commands are : "
        for k in self.__commands.keys():
            print k
