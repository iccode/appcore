""" Web interface for resource_registry.

    This wraps resource_registry and exposes it through HTTP verbs. There
    is a one to one mapping between resource_registry verbs and http verbs.

    That is an HTTP put, will do a resource_registry put etc.

"""

from decimal import Decimal
import json
import datetime
import logging

from sqlalchemy.exc import IntegrityError, DataError
import cherrypy as cp
from cherrypy._cperror import HTTPError

from . import resource_registry
from .resource_registry import _ResourceInstance
from . import UnsupportedActionError, ResourceNotFoundError
from ..session import DbSession


def representation(obj, view='default'):
    if hasattr(obj, 'representation'):
        return obj.representation(view)
    elif isinstance(obj, list):
        return [representation(r, view) for r in obj]
    elif isinstance(obj, dict):
        ret = {k: representation(v, view) for k, v in obj.iteritems()}
        return ret
    else:
        return obj


def encoder(obj):
    if isinstance(obj, (datetime.datetime, datetime.date)):
        return obj.isoformat()
    if isinstance(obj, Decimal):
        return float(obj)

    return obj


def encode_json(obj, indent=True, view='default'):
    """

    :param obj:
    :param indent:
    :param view:
    :return:
    :rtype:
    :raise:
    """
    obj = representation(obj, view)
    try:
        return json.dumps(obj)#, indent=indent, default=encoder)
    except ValueError, err:
        print err
        raise HTTPError(500, err.message)


class ResourceController:
    exposed = True

    def __init__(self):
        self.log = logging.getLogger(self.__class__.__name__)

    def _handle_get(self, path, **kw):
        with DbSession():
            try:
                view = kw.get('_view','default')
                return representation(resource_registry.get(path, **kw), view)
            except DataError, err:
                raise HTTPError(404, err.message)

    def _handle_post(self, path, **kw):
        with DbSession() as sess:
            ln = int(cp.request.headers['Content-Length'])
            ct = cp.request.headers.get('Content-Type', "")
            if not ct.startswith("multipart/form-data"):
                if ln > 0 and cp.request.body is not None:
                    obj = json.loads(cp.request.body.read(ln))
                else:
                    obj = json.loads(kw.pop('data', '{}'))
            else:
                obj = {}
            obj.update(kw)

            try:
                res, add = resource_registry.post(path, **obj)
                if add:
                    sess.add(res)
                    sess.commit()

                return representation(res)
            except IntegrityError, err:
                raise HTTPError(424, err.message)
            except UnsupportedActionError, err:
                raise HTTPError(403, err.message)

    def _handle_delete(self, path, **kw):
        with DbSession() as sess:
            resource_registry.delete(path)
            sess.commit()
            return "ok"

    def _handle_put(self, path, **kw):
        with DbSession() as sess:
            ln = int(cp.request.headers['Content-Length'])
            if ln > 0 and cp.request.body is not None:
                obj = json.loads(cp.request.body.read(ln))
            else:
                obj = json.loads(kw["data"])

            try:
                self.log.debug("Searching for resource %s", path)
                res = resource_registry.put(path, **obj)
                if isinstance(res, _ResourceInstance):
                    if res.isNew:
                        sess.add(res.instance)
                    sess.commit()

                    return representation(res.instance)

                return representation(res)
            except IntegrityError, err:
                raise HTTPError(424, err.message)
            except UnsupportedActionError, err:
                raise HTTPError(403, err.message)

    def __call__(self, *args, **kw):
        req = cp.request
        res = cp.response

        method_override = req.headers.get('X-HTTP-Method-Override', None)
        method = method_override if method_override is not None else req.method
        handler = getattr(self, '_handle_%s' % method.lower())

        try:
            for accept in req.headers.elements('Accept'):
                if accept.value == 'application/json':
                    res.headers['Content-Type'] = 'application/json'
                    return encode_json(handler(*args, **kw))

                if accept.value == 'plain/text':
                    res.headers['Content-Type'] = 'plain/text'
                    return encode_json(handler(*args, **kw), indent=True)

                return encode_json(handler("/".join(args), **kw), indent=True)
        except ResourceNotFoundError:
            raise HTTPError(404)
        except HTTPError:
            raise
        except:
            self.log.error("Cannot process request", exc_info=True)
