from .. import settings
from ..web import get_namespace
from ..auth.models import AppUser, AuthenticationError
from ..base import CPController

import cherrypy as cp
from cherrypy._cperror import HTTPRedirect


class DbAuthController(CPController):
    def __init__(self, login_template):
        self.login_template = login_template

        settings.login_url = self.create_login_url

    def create_login_url(self, redirect_url):
        return '/auth/login?r=' + redirect_url

    def login(self, username=None, password=None, r='/', **kw):
        if cp.request.method == 'GET':
            if not callable(self):
                return self.login_template
            else:
                return self.login_template()

        if cp.request.method == 'POST':

            if not username or not password:
                raise HTTPRedirect('/auth/login?e=Username or password missing')
            try:
                user = AppUser.login(get_namespace(), username, password)
                if hasattr(cp, 'session'):
                    cp.session['user'] = user.key
                    cp.session['email'] = user.username + "@" + user.namespace
                else:
                    assert False  # no session

                raise HTTPRedirect(r)
            except AuthenticationError, e:
                raise HTTPRedirect('/auth/login?e=%s' % e.message)

        pass

    login.exposed = True

    def logout(self):
        if hasattr(cp, 'session'):
            cp.session.delete()
        raise HTTPRedirect('/auth/login')

    logout.exposed = True
