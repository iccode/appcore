from setuptools import setup, find_packages

setup(name='appcore',
      version='1.8.2',
      description='Web infrastructure',
      author='Tasos Vogiatzoglou',
      author_email='tvoglou@iccode.gr',
      packages=find_packages(),
      package_data={},
      install_requires={
          'redis',
          'hiredis',
          'smodels',
          'alembic',
          'bcrypt'
      },
      scripts=[
          'scripts/app-cli'
      ]
)
