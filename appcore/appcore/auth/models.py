import logging
log = logging.getLogger()
from base64 import decodestring, encodestring
from sqlalchemy import orm
from smodels.sa import SAEntity, Text, Array, Boolean, Dictionary
import bcrypt
import cherrypy as cp

MOCK = False

class AuthenticationError(BaseException):
    pass


class AppGroup(SAEntity):
    __tablename__ = 'app_groups'
    __resource__ = {}

    namespace = Text('grpns', primary_key=True)
    code = Text('grpcode', primary_key=True)

    _permissions = Dictionary('grppermissions')
    __perms = {}

    @orm.reconstructor
    def __reconstructor(self):
        self.__perms = {}
        if self._permissions:
            for k, v in self._permissions.iteritems():
                self.__perms[k] = set((v or '').split(';'))

    def __getitem__(self, item):
        return self.__perms.get(item, {})

    def __setitem__(self, key, value):
        if not isinstance(value, (set, list)):
            raise ValueError("Use a set for permissions")

        self.__perms[key] = value

        if self._permissions is None:
            self._permissions = {}

        self._permissions[key] = ";".join(value)

    @property
    def permissions(self):
        return self.__perms

    @classmethod
    def search(cls, **kw):
        code = kw.pop('code', None)
        q = AppGroup.query
        if code:
            q = q.filter(AppGroup.code == code)
        return q

    def bind(self, **kw):
        code = kw.pop('code')
        permissions = kw.pop('permissions')
        self.code = code
        self._permissions = permissions
        if not self.namespace:
            namespace = kw.pop('namespace', 'localhost')
            self.namespace = namespace
        return self

    def representation(self, view='default'):
        d = super(AppGroup, self).representation(view)
        su_dict = dict(
            code=self.code,
            permissions=self._permissions
        )
        d.update(su_dict)
        return d


class AppUser(SAEntity):
    __tablename__ = 'app_users'
    __resource__ = {}

    namespace = Text('usrns', primary_key=True)
    username = Text('usrusername', primary_key=True)

    fullname = Text('usrname')
    password = Text('usrpassword')

    profile = Dictionary('usrdict')

    groups = Array('usrroles', Text)

    is_disabled = Boolean('usrdisabled', default=False)
    is_locked = Boolean('usrlocked', default=False)

    __perms = {}

    @property
    def email(self):
        if '@' in self.username: return self.username

        return self.username+'@'+self.namespace

    @property
    def is_admin(self):
        if not self.groups:
            return False

        return 'admin' in self.groups or 'superadmin' in self.groups


    @orm.reconstructor
    def __reconstructor(self):
        self.__perms = {}
        groups = [AppGroup.load(self.namespace + "|" + g) for g in
                  self.groups or []]

        for g in groups:
            if not g: continue
            for res, perms in g.permissions.iteritems():
                if not res in self.__perms:
                    self.__perms[res] = perms
                else:
                    self.__perms[res] += perms

    def can(self, resource, action):
        if "superadmin" in self.groups: return True
        if not resource in self.__perms: return False
        if not action in self.__perms[resource]: return False
        return True

    @classmethod
    def login(cls, namespace, username, password):
        log.info("Authenticating %s at %s", username, namespace)
        user = cls.one(namespace=namespace, username=username)

        if user is None:
            raise AuthenticationError("Wrong username or password")
        if user.is_disabled:
            raise AuthenticationError("User is disabled")
        if user.is_locked:
            raise AuthenticationError("User is locked")

        if isinstance(password, unicode):
            password = password.encode('utf-8')
        hashed = decodestring(user.password)
        if bcrypt.hashpw(password, hashed) != hashed:
            raise AuthenticationError("Wrong password or username")

        return user

    @classmethod
    def by_email(cls, email):
        return cls.query.filter(AppUser.email==email)

    @classmethod
    def create(cls, namespace, username, password, **kw):

        hashed = bcrypt.hashpw(password, bcrypt.gensalt())

        user = cls(
            namespace=namespace,
            username=username,
            password=encodestring(hashed)
        )

        user.bind(**kw)
        return user

    @classmethod
    def get_current(cls):
        if MOCK:
            return AppUser(namespace='test', username='test')

        usr_key = cp.session.get('user') if hasattr(cp, 'session') else None
        if not usr_key: return None

        return AppUser.load(usr_key)

    def representation(self, view='default'):
        d = super(AppUser, self).representation(view)
        dct = dict(
            username=self.username,
            key=self.key,
            fullname=self.fullname,
            email=self.email,
            profile=self.profile,
            groups=self.groups,
            is_locked=self.is_locked,
            is_admin=self.is_admin,
            is_disabled=self.is_disabled,
            namespace=self.namespace)
        d.update(dct)
        return d
