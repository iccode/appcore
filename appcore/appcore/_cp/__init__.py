from .redis_session import RedisSession
import cherrypy

cherrypy.lib.sessions.RedisSession = RedisSession

