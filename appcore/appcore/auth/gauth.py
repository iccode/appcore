import logging

from .. import settings, get_namespace
from appcore.auth.models import AppUser
from ..base import CPController

from cherrypy._cperror import HTTPRedirect
import cherrypy as cp

GOOGLE_AUTH_ENDPOINT = 'https://accounts.google.com/o/oauth2/auth'
GOOGLE_TOKEN_SOURCE = 'https://accounts.google.com/o/oauth2/token'
GOOGLE_PROFILE_ENDPOINT = 'https://www.googleapis.com/oauth2/v1/userinfo'


class GAuthController(CPController):
    """
    Google Authentication Provider
    """
    log = logging.getLogger("GAuthController")

    def __init__(self, google_client_id, google_client_secret, google_oauth_scope=None):
        self.client_id = google_client_id
        self.client_secret = google_client_secret
        if not google_oauth_scope:
            self.scope = " ".join([
                'https://www.googleapis.com/auth/userinfo.email',
                'https://www.google.com/calendar/feeds/ ',
                'https://www.googleapis.com/auth/userinfo.profile',
                'https://www.googleapis.com/auth/drive'
            ])
        else:
            self.scope = google_oauth_scope

        settings.login_url = self.create_login_url

    def create_login_url(self, redirect_url):
        host = cp.request.headers['Host']
        url = "".join([
            GOOGLE_AUTH_ENDPOINT + "?client_id=" + self.client_id,
            '&redirect_uri=http://%s/auth/cb' % host,
            '&response_type=code',
            '&scope=' + self.scope,
            '&state=%s|%s|%s' % (
                host, redirect_url, get_namespace())
        ])
        return url

    def logout(self):
        if hasattr(cp, 'session'):
            cp.session.delete()
        raise HTTPRedirect('https://accounts.google.com/Logout')

    logout.exposed = True

    def __get_profile__(self, code):
        self.log.debug("Getting profile...")
        import urllib
        import json

        request = {
            'code': code,
            'client_id': self.client_id,
            'client_secret': self.client_secret,
            'grant_type': 'authorization_code',
            'redirect_uri': 'http://%s/auth/cb' % cp.request.headers[
                'Host'],
        }
        self.log.debug("Sending %s", request)

        f = urllib.urlopen(GOOGLE_TOKEN_SOURCE, data=urllib.urlencode(request))
        data = f.read(-1)
        dct = json.loads(data)
        self.log.debug("%s", dct)
        f.close()
        if 'access_token' in dct:
            access_token = dct['access_token']

            f = urllib.urlopen(
                GOOGLE_PROFILE_ENDPOINT + "?access_token=" + access_token)
            profile = json.loads(f.read(-1))
            self.log.debug("Profile is %s" % profile)
            f.close()

            return profile, access_token
        else:
            self.log.debug("Result of authentication was %s" % str(dct))
            return None, None

    def cb(self, code, state=None):
        profile, access_token = self.__get_profile__(code)
        if profile is not None:
            state_tokens = state.split('|')
            s = cp.session if hasattr(cp, 'session') else None
            u = AppUser.load(get_namespace() + "|" + profile['email'])
            s['code'] = code
            s['access_token'] = access_token
            s['user'] = u.key
            s['email'] = profile['email']
            raise cp.HTTPRedirect(state_tokens[1])
        return "Not authenticated"

    cb.exposed = True
