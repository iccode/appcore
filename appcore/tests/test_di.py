from appcore import di


class BaseTest(object):
    def do(self):
        raise NotImplementedError


class BaseTestSub(BaseTest):
    def do(self):
        return 'hello'


def test_register_class():
    di.register(BaseTest, BaseTestSub)

    inst = di.resolve(BaseTest)

    assert inst.do()=='hello'


class ClassWithArgs(object):
    def __init__(self, port):
        self.port =port

def test_make_instance():
    di.register('port', 1000)

    inst = di.make_instance(ClassWithArgs)
    assert inst.port == 1000
