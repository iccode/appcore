import logging
import threading
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import smodels
from smodels import caching

from .versioning.history_meta import versioned_session

log = logging.getLogger(__name__)

_ENGINES = {}

DEBUG = False


def get_engine(url = None):
    """ Returns an SA engine from the thead-local storage. If
        such an engine does not exist, it creates one from 
        the passed url or from smodels.DB_URL method

    """
    url = url or smodels.DB_URL()
    if not url in _ENGINES:
        log.info("No engine, creating a new one to %s"%url)
        engine = create_engine(url, echo=DEBUG)
        _ENGINES[url] = engine

    return _ENGINES[url]


class DbSession():
    """ Provides thread-local, versioned, database-session objects. 
        This class also acts as a context manager, so you can use it 
        like that :

            with DbSession() as sess:
                sess.add(new Entity())
                sess.commit()

    """

    __current = threading.local()

    @staticmethod
    def has_current():
        """ Checks whether we have a session in the current thread """
        return hasattr(DbSession.__current, 'sessions') and smodels.NS_PROVIDER() in DbSession.__current.sessions

    def __init__(self, url=None, engine=None, tag='session', cached=True, versioned=False):


        self.tag = tag
        if not hasattr(DbSession.__current, "sessions"):
            DbSession.__current.sessions = {}

        parts = [self.tag, smodels.NS_PROVIDER()]
        if cached:
            parts.append("cached")
        if versioned:
            parts.append("versioned")

        self.__key = "::".join(parts)

        if not self.__key in DbSession.__current.sessions:
            engine = engine or get_engine(url)
            if not cached:
                Session = sessionmaker(bind=engine, autoflush=False)
            else:
                Session = sessionmaker(bind=engine, autoflush=False,
                                       query_cls=caching.query_callable(
                                           smodels.CACHE_REGIONS))

            if versioned:
                versioned_session(Session)

            self._close = True

            DbSession.__current.sessions[self.__key] = Session()
        else:
            self._close = False

    def close(self):
        if self._close:
            self.session.close()
            DbSession.__current.sessions.pop(self.__key, None)

    def __getattr__(self, item):
        s = DbSession.__current.sessions.get(self.__key, None)
        if item=="session":
            return s

        if not s:
            raise Exception("Session has expired")

        return getattr(s,item)

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        if traceback:
            self.session.rollback()
        self.close()


