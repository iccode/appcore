import logging
from cherrypy import Tool
import cherrypy
from sqlalchemy.orm import scoped_session, sessionmaker


class SATool(Tool):
    log = logging.getLogger("spnd.cp.SATool")

    def __init__(self,engine_provider):
        """
        The SA tool is responsible for associating a SA session
        to the SA engine and attaching it to the current request.
        Since we are running in a multithreaded application,
        we use the scoped_session that will create a session
        on a per thread basis so that you don't worry about
        concurrency on the session object itself.

        This tools binds a session to the engine each time
        a requests starts and commits/rollbacks whenever
        the request terminates.
        """
        #Tool.__init__(self,,self.bind_session)
        self._point = 'before_handler'
        self._name = 'SATool'
        self._priority = 10
        self.engine_provider = engine_provider

    def callable(self):
        self.log.debug("Binding db session")
        setattr(
            cherrypy.serving.request,
            "db",
            scoped_session(sessionmaker(bind=self.engine_provider(),autoflush=False))
        )
        cherrypy.serving.request.hooks.attach('on_end_resource',
            self.close_session,
            priority=80)

    def close_session(self):
        if hasattr(cherrypy.request,'db'):
            self.log.debug("Clearing db session")
            cherrypy.request.db.close()
            cherrypy.request.db.remove()
            cherrypy.request.db = None


