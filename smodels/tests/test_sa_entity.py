from smodels.sa import SAEntity
from smodels.session import DbSession, get_engine
from tests.resources_sa import Case, Action, MultiPK, \
    VersionedItem, VersionedItemChild

from fixtures import  engine

def setup_module(module):
    engine = get_engine()
    SAEntity.metadata.create_all(bind=engine)
    engine.dispose()

def teardown_module(module):
    engine = get_engine()
    for t in SAEntity.metadata.sorted_tables:
        engine.execute('DROP TABLE %s CASCADE'%t.description)
    engine.dispose()

def test_insert(engine):
    with DbSession(engine=engine) as sess:
        c = Case(title='Test title')
        c.actions.append(Action(title='Test action'))
        sess.add(c)
        sess.commit()

        v =  Case.load(c.key)

        assert c.id is not None
        assert v.title == 'Test title'
        assert len(v.actions)==1

def test_multipk(engine):
    with DbSession(engine=engine) as sess:
        o = MultiPK()
        o.part1 = "tes"
        o.part2 = "test3"
        o.part3 = "me"

        sess.add(o)
        sess.commit()

        ol = MultiPK.load(o.key)
        assert ol is not None

        str_repr = str(ol)
        assert str_repr == 'MultiPK(%s,%s,%s)'%(o.part1, o.part2, o.part3)

        v = sess.query(MultiPK.key_func).limit(1).scalar()
        assert v == o.key

        o = MultiPK.load('a||')
        assert o is None

def test_delete(engine):
    with DbSession(engine=engine) as sess:
        o = MultiPK()
        o.part1 = "tes"
        o.part2 = "test3"
        o.part3 = "me"

        sess.add(o)
        sess.commit()

        assert len(MultiPK.all())==1

        o.delete()
        sess.commit()


        assert len(MultiPK.all())==0



def test_delete_children(engine):
    with DbSession(engine=engine) as sess:
        c = Case(title='Test title')
        c.actions.append(Action(title='Test action'))
        sess.add(c)
        sess.commit()

        v =  Case.load(c.key)

        assert v.title == 'Test title'
        assert len(v.actions)==1

        v.delete()
        sess.commit()

        assert Case.load(c.key) is None
        assert len(Action.all()) == 0




def test_versioning(engine):
    with DbSession(engine=engine) as sess:
        v = VersionedItem()
        v.items.append(VersionedItemChild(
            title='test'
        ))

        sess.add(v)
        sess.commit()

        l = VersionedItem.load(v.key)
        assert l is not None
        assert l.version == 1

        l.items.append(VersionedItemChild(
            title='test 2'
        ))

        sess.commit()

        l = VersionedItem.load(v.key)

        assert len(l.items)==2



