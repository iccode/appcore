import cherrypy as cp
from appcore.utils import parse_domain
from . import _cp

NAMESPACE_KEY = "namespace"
_NS_ = None


def set_namespace(value):
    global _NS_
    if hasattr(cp, 'session'):
        sess = cp.session
        if sess:
            sess[NAMESPACE_KEY] = value
        else:
            _NS_ = value
    else:
        _NS_ = value


def get_namespace():
    global _NS_
    if _NS_ is not None: return _NS_

    if "Host" in cp.request.headers:
        netloc = cp.request.headers['Host'].split(':')[0]
    else:
        netloc = "default"

    ns = parse_domain("http://" + netloc, levels=3)

    sess = cp.session if hasattr(cp, 'session') else None
    if sess is None: return ns
    if NAMESPACE_KEY in sess and len(sess[NAMESPACE_KEY]) > 0:
        return sess[NAMESPACE_KEY]

    return ns


def make_app(root, config=None):
    from . import settings
    import cherrypy

    if not 'tools.sessions.redis_pool' in config:
        settings.CP_CONFIG['tools.sessions.redis_pool'] = settings.redis_pool

    cherrypy.config.clear()
    cherrypy.config.update(settings.CP_CONFIG)
    app = cherrypy.tree.mount(root, config=config)

    return app
