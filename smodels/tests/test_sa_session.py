from Queue import Queue
from threading import Thread
import time
import pytest
from smodels.session import DbSession

def test_DbSession_thread_locality():

    #test we get the same session between two calls
    s1 = DbSession()

    time.sleep(1)

    s2 = DbSession()

    assert id(s1.session) == id(s2.session)
    assert s1 is not s2

    #test second session doesn't close the connection
    s2.close()
    assert s1.session.is_active

    class OtherThread(Thread):
        def __init__(self, q):
            super(OtherThread, self).__init__()
            self.q = q

        def run(self):
            sess = DbSession()
            self.q.put(sess.session)


    q = Queue()
    th = OtherThread(q)
    th.start()
    th.join()

    #test session creation time is different from other thread
    thread_session = q.get(block=True)
    assert thread_session is not s1.session

    #test first session closes the connection
    s1.close()
    assert s1.session is None

def test_DbSession_context_manager():
    ## Test proper workings of context manager
    with DbSession() as sess:

        newSession = DbSession()
        assert newSession.session is sess.session

    assert sess.session is None
    assert newSession.session is None

    with pytest.raises(Exception):
        newSession.add.query()


def test_DbSession_multiple_tags():
    ## Test we get different objects per tag
    with DbSession(tag="a") as aSess:
        b = DbSession()
        assert id(b.session) != id(aSess.session)

    assert aSess.session is None
    assert b.session is not None
