import pytest
from smodels.sa import SAEntity
from smodels.session import get_engine


def setup_module(module):
    engine = get_engine()
    SAEntity.metadata.create_all(bind=engine)
    engine.dispose()


def teardown_module(module):
    engine = get_engine()
    for t in SAEntity.metadata.sorted_tables:
        engine.execute('DROP TABLE %s CASCADE'%t.description)
    engine.dispose()


@pytest.fixture()
def engine(request):
    engine = get_engine()

    con = engine.connect()
    with con.begin() as trans:
        for table in reversed(SAEntity.metadata.sorted_tables):
            try:
                con.execute(table.delete())
            except:
                pass
        trans.commit()
    con.close()
    def _clear_all():
        engine.dispose()

    request.addfinalizer(_clear_all)
    return engine
