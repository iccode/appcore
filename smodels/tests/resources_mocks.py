import time
from smodels.resources import ResourceBase
from smodels.resources import collection_of

__author__ = 'flatline'

class Case(ResourceBase):
    __resource__ = {
    }

    id = ""
    name = ""

    def __init__(self,**kw):
        for k,v in kw.items():
            setattr(self,k,v)

    @collection_of('actions')
    def actions(self, key=None):
        def _make_action(i):
            return Action(
                id=i,
                name='test_%s'%i,
                case=self
            )
        if key and key!="666":
            return _make_action(key)
        elif key=="666":
            return None

        return map(_make_action, range(1,10))

    @actions.appender
    def make_action(self, **kw):
        pass


    def bind_action(self, key=None, **kw):
        pass

    def delete_action(self, key):
        pass

    @staticmethod
    def load(key):
        s = Case(
            id=key,
            name='test_%s'%key
        )
        return s

    @staticmethod
    def all():
        def _make_case(i):
            return Case(
                id=i,
                name='test_%s'%i
            )
        return map(_make_case, range(0,10))

class AutoId(ResourceBase):
    __resource__ = {}
    _autoid = True

    id = 1
    name = None

    def __init__(self, *args, **kw):
        self.id = time.time()

        for k,v in kw.items():
            setattr(self,k,v)


class Action(ResourceBase):
    __resource__ = {}

    id = 0
    name= None

    def __init__(self,**kw):
        for k,v in kw.items():
            setattr(self,k,v)

        if not hasattr(self,"id"):
            self.id = ""
            self.name = ""

    @collection_of('auto_ids')
    def autoids(self):
        def _make_auto():
            i = AutoId()
            i.case = self
            return i

        return _make_auto()

    def add_autoids(self, key=None, **kw):
        return AutoId(id=10, **kw)

    @staticmethod
    def all():
        def _make_action(i):
            return Action(
                id=i,
                name='test_%s'%i
            )
        return map(_make_action, range(0,10))
