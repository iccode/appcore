""" Resource registry implements the functionality of the resources management.

"""
from collections import namedtuple
import re
import inflect
from . import ResourceNotFoundError, UnsupportedActionError
from sqlalchemy import or_

inflector = inflect.engine()

RESOURCES = {}

no_internal = lambda x: x[0:2] != '__' and x[:2] != '__'
no_special = lambda x: x[0] != '_' and x[-1] != '_'


def _make_name(nm):
    """
    Makes a resource name from a classname. As PEP compliant classes are named
    in capital camecase we do the following transformation
    HistoryItem -> history_items

    Note that the last word is capitalized denoting that we give PLURAL names
    to the resources
    """
    words = re.findall('[A-Z][^A-Z]*', nm)
    if len(words) == 0:
        return inflector.plural(nm.lower())

    words[-1] = inflector.plural(words[-1])
    return "_".join([w.lower() for w in words])


def _make_single_name(nm):
    """
    Makes a singular name from a resource name.
    """
    words = nm.split('_')
    if len(words) == 1:
        return inflector.singular_noun(words[0])
    words[-1] = inflector.singular_noun(words[-1])
    return "_".join(map(str, words))


def _make_search(class_):
    """ Creates a search method if one does not exist in the class.
        The search method is way to perform queries given a dictionary
        or list of predicates.

    """
    def search(**kw):
        q = class_.query
        if 'preds' in kw and isinstance(kw['preds'], list):
            return class_.query

        for key, value in kw.iteritems():
            if not hasattr(class_, key): continue
            prop = getattr(class_, key)
            if isinstance(value, list):
                clause = or_(*[prop==v for v in value])
                q = q.filter(clause)
            else:
                q = q.filter(prop == value)
            pass
        return q

    return search


class Resource(object):
    """
    Class representing a resource
    """

    def __init__(self, class_):
        assert isinstance(class_, type)

        cname = class_.__name__
        self.name = class_.__resource__.get('name', _make_name(cname))
        self.single = _make_single_name(self.name)

        #we mutate the original name or add the new one
        class_.__resource__['name'] = self.name

        resource = class_.__resource__

        def try_id(id_):
            if hasattr(class_, id_):
                self.id = id_
                return True

            return False

        for id_ in ['id', 'key', 'code']:
            if try_id(id_): break

        self.base_url = resource.get('url', '/%s/' % self.name)
        self.load = resource.get('load', class_.load)
        self.all = resource.get('all', class_.all)
        if hasattr(class_, 'search'):
            self.search = class_.search
        else:
            self.search = resource.get('search', _make_search(class_))

        self.delete = resource.get('delete', class_.delete)
        self.bind = resource.get('bind', class_.bind)
        self.type = class_
        self.collections = self.__get_collections(class_)
        self.actions = self.__get_actions(class_)

    def representation(self, view='default'):
        return {
            a: getattr(self, a) for a in
            filter(lambda x: no_internal(x) and no_special(x), dir(self))
            if not callable(getattr(self, a))
        }

    def create(self, key=None, **kw):
        t = self.type()
        if key is not None: setattr(t, self.id, key)
        return t

    @staticmethod
    def __get_collections(class_):
        """
        Returns the resources collections under this class
        """
        colls = {}
        for a in filter(no_internal, dir(class_)):
            attr = getattr(class_, a)
            if hasattr(attr, '_collection'):
                colls[a] = getattr(attr, '_collection')

        return colls

    @staticmethod
    def __get_actions(class_):
        """
        Returns the actions supported by this resource
        """
        actions = {}
        for a in dir(class_):
            attr = getattr(class_, a)
            if callable(attr) and hasattr(attr, "_action"):
                actions[a] = getattr(attr, '_action')

        return actions


#region Things returned by find_resource
class _CollectionInstance(namedtuple("CollectionInstance", ['resource',
                                                            'instance',
                                                            'collection',
                                                            'parent'])):
    def create_item(self, key=None, **params):
        res = RESOURCES[self.resource]

        try:
            auto = res.type._has_auto_key()
        except:
            auto = hasattr(res.type, '_autoid')

        if not auto and key is None:
            raise UnsupportedActionError(
                "Cannot bind %s because key is required" % res.name)

        if auto and key is not None:
            raise UnsupportedActionError(
                "Cannot bind %s with key %s because has autoid" % res.name)

        # check if there is an add_<resource> and invoke it
        add_method = 'add_%s' % self.collection
        if hasattr(self.instance, add_method):
            add = getattr(self.instance, add_method)
            if not callable(add):
                raise Exception("Collection adder is not a callable")

            res = add(self.instance, **params)
            return res

        # try to make the resource and call the append method of the collection
        col = getattr(self.instance, self.collection)
        if hasattr(col, 'append'):
            add = getattr(col, 'append')
            if not callable(add):
                raise Exception("Collection added is not callable")

            res = res.create(key)
            res.bind(**params)
            add(res)
            return res

        raise UnsupportedActionError(
            "Don't know how to add to collection %s" % self.collection)

    def get_item(self, key):
        return getattr(self.instance, self.collection)(key=key)

    def get_all(self):
        collection = getattr(self.instance, self.collection)
        return collection() if callable(collection) else collection


class _ResourceInstance(namedtuple("ResourceInstance", ["resource",
                                                        "instance",
                                                        "isNew",
                                                        "parent"])
):
    pass


class _ActionInstance(namedtuple("ActionInstance", ["resource",
                                                    "instance",
                                                    "action",
                                                    "parent"])):
    def invoke(self, *args, **kw):
        method = getattr(self.instance, self.action)

        res = method(*args, **kw)

        return res

#endregion


def register(class_):
    resource = Resource(class_)
    RESOURCES[resource.name] = resource


def find_resource(path, create_missing=False):
    """
    The actual mechanism for resolving a resource from its path

    """

    def _find(head, rest, parent=None):
        inEnd = len(rest) == 0

        if parent is None:
            if not head in RESOURCES:
                raise ResourceNotFoundError(head)

            parent = RESOURCES[head]
            if inEnd:
                return parent
            else:
                return _find(rest[0], rest[1:], parent)

        if isinstance(parent, Resource):
            inst = parent.load(head)
            if inst is None and not create_missing:
                raise ResourceNotFoundError(path)

            elif inst is None and create_missing:
                inst = parent.create(key=head)
                res = _ResourceInstance(resource=parent.name,
                                        instance=inst,
                                        isNew=True, parent=parent)
            else:
                res = _ResourceInstance(resource=parent.name,
                                        instance=inst,
                                        isNew=False, parent=parent)

            if inEnd:
                return res
            else:
                return _find(rest[0], rest[1:], res)

        elif isinstance(parent, _ResourceInstance):
            resource = RESOURCES[parent.resource]
            if head in resource.collections:
                instance = _CollectionInstance(
                    resource=resource.collections[head].of,
                    instance=parent.instance,
                    collection=head,
                    parent=parent)

                if inEnd:
                    return instance
                else:
                    return _find(rest[0], rest[1:], instance)
            elif head in resource.actions and inEnd:
                instance = _ActionInstance(
                    resource=resource,
                    instance=parent.instance,
                    action=head,
                    parent=parent
                )
                return instance

            raise UnsupportedActionError(path)

        elif isinstance(parent, _CollectionInstance):
            item = parent.get_item(head)

            #we can only create the last component of the URI
            if inEnd and create_missing and item is None:
                resource = RESOURCES[parent.resource]
                res = _ResourceInstance(
                    resource=parent.resource,
                    instance=parent.create_item(head),
                    isNew=True,
                    parent=parent
                )
                return res

            if item is None:
                raise ResourceNotFoundError(path)

            res = _ResourceInstance(resource=parent.resource,
                                    instance=item, isNew=False,
                                    parent=parent)

            if inEnd:
                return res
            else:
                return _find(rest[0], rest[1:], res)

        assert False  # We shouldn't be here

    toks = path.split('/')
    return _find(toks[0], toks[1:])


def get(path, **kw):
    """
    Get a resource by it's path
    """
    path_toks = filter(lambda x: len(x), path.split('/'))
    if not len(path_toks):
        return RESOURCES.values()

    resource = find_resource("/".join(path_toks))
    if resource is None:
        raise ResourceNotFoundError(path)
    elif isinstance(resource, Resource):
        if not kw:
            return resource.all()
        else:
            return resource.search(**kw).all()

    elif isinstance(resource, _CollectionInstance):
        return resource.get_all()
    elif isinstance(resource, _ResourceInstance):
        return [resource.instance]
    else:
        return resource


def put(path, **params):
    """
    Create a new resource in the path
    """
    path_toks = filter(lambda x: len(x), path.split('/'))
    if not len(path_toks):
        raise UnsupportedActionError("Cannot post in root")

    resource = find_resource("/".join(path_toks), create_missing=True)
    if resource is None:
        raise ResourceNotFoundError(path)
    elif isinstance(resource, _CollectionInstance):
        raise UnsupportedActionError("Cannot put at uri %s" % path)
    elif isinstance(resource, _ResourceInstance):
        resource.instance.bind(**params)
        return resource

    raise UnsupportedActionError("Don't know what to do with %s" % path)


def delete(path):
    """
    Deletes a resource at the path
    """
    path_toks = filter(lambda x: len(x), path.split('/'))

    if not len(path_toks):
        raise UnsupportedActionError("Cannot delete in root")

    resource = find_resource("/".join(path_toks))
    if resource is None:
        raise ResourceNotFoundError(path)
    elif isinstance(resource, _CollectionInstance):
        raise UnsupportedActionError("Cannot delete at uri %s" % path)
    elif isinstance(resource, _ResourceInstance):
        return resource.instance.delete()

    raise UnsupportedActionError("Don't know what to do with %s" % path)


def post(path, **params):
    """
    Posts a resource to a url
    """
    path_toks = filter(lambda x: len(x), path.split('/'))
    if not len(path_toks):
        raise UnsupportedActionError("Cannot post in root")

    resource = find_resource("/".join(path_toks))
    if resource is None:
        raise ResourceNotFoundError(path)
    elif isinstance(resource, Resource):
        r = resource.type()
        r.bind(**params)
        return r, True
    elif isinstance(resource, _CollectionInstance):
        r = resource.create_item(**params)
        return r, True
    elif isinstance(resource, _ActionInstance):
        result = getattr(resource.instance, resource.action)(**params)
        return result, False

    raise UnsupportedActionError("Don't know what to do with %s" % path)
