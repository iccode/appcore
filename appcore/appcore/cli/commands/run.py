from argparse import ArgumentParser

parser = ArgumentParser(description='Runs the application')
parser.add_argument("port", type=int, default=8080, help='The port to run')


def command(**kw):
    import cherrypy as cp

    cp.config.update({'server.socket_port': kw['port'], })
    cp.engine.signals.subscribe()
    cp.engine.start()
    cp.engine.block()


command.parser = parser

