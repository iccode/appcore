from threading import Thread
import time
import requests
from appcore import di, settings
from appcore.auth.nsauth import NamespaceAuthController

import cherrypy as cp

import random
from smodels.sa import SAEntity
from smodels.session import get_engine, DbSession
from .fixtures import engine

port = random.randint(10000, 11000)

import appcore.auth.models as am

di.register('login_template', 'tmpl')
di.register('google_client_id', 'tmpl')
di.register('google_client_secret', 'tmpl')


def _u(part):
    return "http://localhost:%d/%s" % (port, part)


def _start_server():
    import cherrypy

    class Root():
        auth = NamespaceAuthController()

        def index(self):
            return "hello world"

        index.exposed = True

    cherrypy.config.update(settings.CP_CONFIG)
    cherrypy.config.update({'server.socket_port': port})
    cherrypy.tree.mount(Root())
    cherrypy.engine.start()
    cherrypy.engine.block()


def setup_module(module):
    engine = get_engine()
    SAEntity.metadata.create_all(bind=engine)
    engine.dispose()
    module.proc = Thread(target=_start_server)
    module.proc.start()
    time.sleep(1)


def teardown_module(module):
    cp.engine.exit()
    module.end = True

    engine = get_engine()
    for t in SAEntity.metadata.sorted_tables:
        engine.execute('DROP TABLE %s CASCADE' % t.description)
    engine.dispose()


def test_db_login(engine):
    settings.ns('localhost').auth_mode = 'db'
    with DbSession(engine=engine) as s:
        group = am.AppGroup(namespace="ns", code='grp')
        group['res'] = {'edit', 'delete'}

        s.add(group)

        u = am.AppUser.create('localhost', 'user', 'pass')
        u.groups = ['grp']
        s.add(u)

        s.commit()

    r = requests.get(_u('/auth/login'))
    assert r.content == "tmpl"

    r = requests.post(_u('/auth/login'),
                      {'username': 'user', 'password': 'pass'})
    assert r.content == "hello world"


