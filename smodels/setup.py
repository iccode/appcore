from setuptools import setup, find_packages

setup(name='smodels',
      version='1.8.2',
      description='Models and REST infrastructure',
      author='Tasos Vogiatzoglou',
      author_email='tvoglou@iccode.gr',
      packages = find_packages(),
      package_data = {},
      install_requires=[
          "sqlalchemy==0.9.1",
          "i18n",
          "dateutils",
          "inflect",
          "dogpile.cache",
          'redis', 'bcrypt', 'cherrypy'
      ],
)
