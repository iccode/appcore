""" Utilities and classes around SqlAlchemy
"""
import traceback
from sqlalchemy import literal_column, MetaData, Column, text
from sqlalchemy.dialects.postgresql import HSTORE, ARRAY
from sqlalchemy.ext.declarative import DeclarativeMeta, declarative_base
from sqlalchemy.orm import class_mapper, object_mapper, object_session, \
    relationship, RelationshipProperty
from sqlalchemy.orm.attributes import InstrumentedAttribute
from sqlalchemy.orm.collections import InstrumentedList
from sqlalchemy.sql import ColumnElement
import warnings
import datetime

from .resources import ResourceMeta, _collection_of
from .session import DbSession

class RelMeta(ResourceMeta,DeclarativeMeta):
    """ A metaclass that inherits ResourceMeta and DeclarativeMeta, thus
        providing a common metaclass for resources and database entities.
    """
    def __init__(cls, name, bases, dct):
        ResourceMeta.__init__(cls,name, bases, dct)
        if cls.__name__=='SAEntityBase':
            return

        valid_fields = set()
        for k,v in dct.items():
            if k[:2]=="__" or k in SPECIAL or k[-1]=="_" or k[0]=="_": continue
            if isinstance(v, (property,Column, RelationshipProperty)):
                valid_fields.add(k)

        for b in bases:
            if hasattr(b,'properties'):
                valid_fields.update(b.properties)

        valid_fields.update({'key','resURL'})

        setattr(cls, 'properties',frozenset(valid_fields))


    def __get_valid_fields(cls):
        if not hasattr(cls, '__valid_fields'):
            cls.__valid_fields = filter(lambda x: no_internal(x) and no_special(x), dir(cls))
        return cls.__valid_fields
    _valid_fields = property(__get_valid_fields)


    def __query(cls):
        return DbSession().query(cls)
    query = property(__query)

    def select(cls, *args):
        return DbSession().query(*args)

    def __key_func(cls):
        """ Returns a query fragment that recreates the objects key in sql
            So, if we have an entity with a compound primary key

                pk1 = Text('id1', primary_key=True)
                pk2 = Text('id2', primary_key=True)

            this function returns a sql framegment that does (on the database side)
            (id1 || '|' || id2) so we can do stuff with it, like searching and displaying it
        """
        pk = class_mapper(cls).primary_key
        pk_fun = None
        for i,p in enumerate(pk):
            if pk_fun is None:
                pk_fun = p
            else:
                pk_fun = pk_fun + p

            if i +1 < len(pk):
                pk_fun = pk_fun +literal_column("'|'")

        return pk_fun
    key_func = property(__key_func)

SPECIAL = {'session','metadata','all','query','delete','bind','load',
           'many','representation','set_key','get_key','one',
           '_decl_class_registry'}
SPECIAL_TYPES = {MetaData}

no_internal = lambda x: x[0:2]!='__' and x[:2]!='__'
no_special = lambda x: x[0]!='_' and x[-1]!='_' and x not in SPECIAL and not isinstance(x,tuple(SPECIAL_TYPES))

class RelEntity(object):
    """Base class for DB entities"""

    @property
    def resURL(self):
        if hasattr(self, '__resource__'):
            return "%s/%s"%(self.__resource__['name'],self.key)
        else:
            return None

    def __hash__(self):
        if hasattr(self,'__key') : return self.__key
        self.__key =  self.key
        return self.__key

    def delete(self):
        self.session.delete(self)
        return self

    def bind(self, **params):
        for k,v in params.items():
            try:
                if v is not None:
                    prop = getattr(self.__class__, k)
                    if not isinstance(prop, InstrumentedAttribute): continue
                    cols = prop.property.columns
                    if len(cols)==1:
                        col = cols[0]
                        if not isinstance(col, ArrayColumn):
                            setattr(self, k, v)
                        else:
                            if not isinstance(v, list):
                                setattr(self, k, [v])
                            else:
                                setattr(self, k, v)
            except:
                warnings.warn("Unable to set %s"%k)
                print traceback.print_exc()

    def representation(self, view='default'):
        def _encode(o, view):
            if isinstance(o, (list, InstrumentedList)):
                return [_encode(oi, view) for oi in o]
            elif isinstance(o, dict):
                return {k:_encode(v, view) for k,v in o.items()}
            elif hasattr(o,'representation'):
                return o.representation(view)
            elif isinstance(o, datetime.datetime):
                return o.strftime('%Y-%m-%dT%H:%M:%S.%f%z')
            else:
                return o


        ret = {
            '__url__':self.resURL
        }
        for k in self.__class__.properties:
            v = getattr(self, k)
            if isinstance(v, (list, InstrumentedList)):
                ret[k] = _encode(v, view)
            else:
                ret[k] = _encode(v, view)

        return ret


    def __json__(self):
        return self.representation()

    def get_key(self):
        mapper = object_mapper(self)
        k = mapper.primary_key_from_instance(self)
        if len(k)==1: return k[0]
        return "|".join(map(unicode,k))

    def set_key(self, key):
        ktoks = key.split('|')
        k = self._get_pk()
        for i,p in enumerate(k):
            setattr(self,p.key,ktoks[i])

    key = property(get_key, set_key)

    def __str__(self):
        cls = self.__class__.__name__
        mapper = object_mapper(self)
        k = mapper.primary_key_from_instance(self)
        fix = lambda x: "" if x is None else x
        k = map(fix,k)
        str_key = ",".join(map(str,k))
        str_key = "" if str_key is None else str_key
        return "%s(%s)"%(cls,str_key)

    __repr__ = __str__

    @property
    def session(self):
        """Returns the session that owns the current instance"""
        return object_session(self)

    @classmethod
    def _get_pk(cls):
        return class_mapper(cls).primary_key

    @classmethod
    def _has_auto_key(cls):
        keys = cls._get_pk()
        for k in keys:
            if k.autoincrement: return True
        return False

    @classmethod
    def one(cls,**kw):
        """ Makes a query and returns the first result (if any).

            :param kw: properties of the class
            :return: Returns the first item specified by the arguments
            :rtype: cls
        """
        q = cls.query
        for k,v in kw.iteritems():
            q = q.filter(cls.__dict__[k]==v)

        return q.first()

    @classmethod
    def many(cls,**kw):
        """
        :param kw: properties of the class
        :return: Returns the first item specified by the arguments
        :rtype: cls
        """
        q = cls.query
        for k,v in kw.iteritems():
            if k in cls.__dict__:
                q = q.filter(cls.__dict__[k]==v)

        return q.all()

    @classmethod
    def __load_by_pk(cls,*args):
        q = cls.query
        keys = cls._get_pk()

        if len(args)==len(keys):
            for v,i in enumerate(keys):
                q = q.filter(i==args[v])
            return q.first()

        return None

    @classmethod
    def load(cls,key):
        """
        Loads the entity specified by key
        :param key: The key to load
        :return:
        """
        if isinstance(key,basestring) and '|' in key:
            return cls.__load_by_pk(*key.split('|'))
        else:
            return cls.__load_by_pk(key)

    @classmethod
    def all(cls):
        return cls.query.all()

SAEntity = declarative_base(name='SAEntityBase',cls=RelEntity,metaclass=RelMeta)


def collection(type, **kw):
    r = relationship(type,**kw)
    r._collection = _collection_of(type)
    return r


#
# Some helpers/shortcuts for sqlalchemy
#

import sqlalchemy as sa

from sqlalchemy.dialects import postgresql

class ArrayElement(ColumnElement):
    def contains(self,value,**kw):
        if isinstance(value,basestring):
            return text("%s @> ARRAY['%s']"%(self,value))
        if isinstance(value,list):
            return text("%s && ARRAY[%s]"%(self,",".join(["'%s'"%s for s in value])))
        else:
            return text("%s @> ARRAY[%s]"%(self,value))


class ArrayColumn(ArrayElement,Column):
    pass

def Array(name,type,*args,**kw):
    if type is Text:
        return ArrayColumn(name,postgresql.ARRAY(sa.Text),*args,**kw)

def Boolean(name,*args,**kw):
    return Column(name,sa.Boolean,*args,**kw)

def Text(name,*args,**kwargs):
    if "max" in kwargs:
        max = int(kwargs.pop('max'))
        return Column(name,sa.String(max),*args,**kwargs)

    return Column(name,sa.Text,*args,**kwargs)

def Integer(name,*args,**kw):
    return Column(name,sa.Integer,*args,**kw)

def Decimal(name,*args,**kw):
    return Column(name,sa.Numeric,*args,**kw)

def StringChoice(name,*args,**kw):
    choices = kw.pop('choices',[])
    multiple = kw.pop('multiple',False)
    if not multiple:
        return Column(name,sa.Text,*args,**kw)
    else:
        return Array(name,Text,*args,**kw)

def Dictionary(name=None, *args, **kw):
    if name is None:
        return Column(HSTORE, *args, **kw)
    else:
        return Column(name, HSTORE, *args, **kw)


def Dictionaries(name=None, *args, **kw):
    if name is None:
        return Column(ARRAY(HSTORE),*args, **kw)
    else:
        return Column(name, ARRAY(HSTORE), *args, **kw)
