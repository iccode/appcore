from smodels.sa import SAEntity
from smodels.session import DbSession, get_engine
from smodels.resources.web import encode_json
from tests.resources_sa import Case, Action, MultiPK,\
    VersionedItem, VersionedItemChild

from fixtures import  engine

def setup_module(module):
    engine = get_engine()
    SAEntity.metadata.create_all(bind=engine)
    engine.dispose()

def teardown_module(module):
    engine = get_engine()
    for t in SAEntity.metadata.sorted_tables:
        engine.execute('DROP TABLE %s CASCADE'%t.description)
    engine.dispose()

def test_circular(engine):
    with DbSession(engine=engine) as sess:
        for i in range(1,10):
            c = Case(title='_%d'%i)
            for z in range(1,10):
                c.actions.append(Action(title='action_%d'%z))

            sess.add(c)
            sess.commit()

        a = Case.all()
        assert a[0].actions[0].parent_case is not None
        json = encode_json(a)

        print json

        assert json is not None

