from sqlalchemy import Column, Integer, Text, ForeignKey
from sqlalchemy.orm import relationship
from smodels.resources import action
from smodels.sa import SAEntity, collection
from smodels.session import DbSession
from smodels.versioning.history_meta import Versioned


class Action(SAEntity):
    __tablename__ = 'action'
    __resource__ = {}

    id = Column(Integer, primary_key=True)
    title = Column(Text)

    case_id = Column(Integer, ForeignKey('cases.id'))


class Case(SAEntity):
    __tablename__ = 'cases'
    __resource__ = {}

    id = Column(Integer, primary_key=True, autoincrement=True)
    title = Column(Text)

    actions = collection(Action, cascade='all', backref='parent_case')

    @action()
    def clear_actions(self, answer="Ok"):
        with DbSession() as sess:
            for c in self.actions:
                sess.delete(c)
            sess.commit()

        return answer


class Customer(SAEntity):
    __tablename__ = 'customers'
    __resource__ = {}

    fiscal = Column(Text, primary_key=True)
    name = Column(Text, nullable=False)


class MultiPK(SAEntity):
    __tablename__ = 'multi_pk_tests'
    __resource__ = {}

    part1 = Column(Text, primary_key=True)
    part2 = Column(Text, primary_key=True)
    part3 = Column(Text, primary_key=True)


class VersionedItemChild(SAEntity):
    __tablename__ = 'versioned_entity_children'
    __resource__ = {}

    id = Column(Integer, primary_key=True)
    title = Column(Text)

    case_id = Column(Integer, ForeignKey('versioned_entity.id'))


class VersionedItem(SAEntity, Versioned):
    __tablename__ = 'versioned_entity'
    __resource__ = {}

    id = Column(Integer, primary_key=True, autoincrement=True)
    items = relationship(VersionedItemChild, cascade='all')


