class NotSetError(Exception):
    pass

import warnings

class _SettingsDict(object):
    def __init__(self, ns, fallback=None):
        object.__setattr__(self, '__values__', {})
        object.__setattr__(self, 'ns', ns)
        object.__setattr__(self, '__fallback__', fallback)

    def __setattr__(self, item, value):
        self.__values__[item] = value

    def __getattr__(self, item):
        if not item in self.__values__ and self.__fallback__:
            value = getattr(self.__fallback__, item)

            if value is None:
                raise NotSetError(item)

            warnings.warn("Configuration %s not found in namespace %s. Falling back to default"%(item, self.ns))
            return value

        return self.__values__[item]

    def update(self, cfg):
        if not isinstance(cfg, dict):
            raise ValueError("Dictionary is needed")

        self.__values__.update(cfg)

    def has(self, item):
        return item in self.__values__


class _Settings(object):
    __namespaces__ = {
    }

    def __init__(self):
        object.__setattr__(self, '_default', _SettingsDict('default', {}))

    def __getattr__(self, item):
        return getattr(self._default, item)

    def __setattr__(self, item, value):
        setattr(self._default, item, value)

    def ns(self, namespace):
        if namespace=='default':
            return self._default

        if not namespace in self.__namespaces__:
            self.__namespaces__[namespace] = _SettingsDict(namespace, self._default)

        return self.__namespaces__[namespace]

    @property
    def current_ns(self):
        from appcore import get_namespace

        ns = get_namespace()
        if ns == "localhost" or ns not in self.__namespaces__:
            ns = 'default'

        return self.ns(ns)

