#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2007-2011 Edgewall Software
# All rights reserved.
#
# This software is licensed as described in the file COPYING, which
# you should have received as part of this distribution. The terms
# are also available at http://i18n.edgewall.org/wiki/License.
#
# This software consists of voluntary contributions made by many
# individuals. For the exact contribution history, see the revision
# history and logs, available at http://i18n.edgewall.org/log/.

from setuptools import setup, find_packages

setup(
    name = 'i18n',
    version = '1.8.2',
    description = 'Internationalization utilities',
    long_description = "A collection of tools for internationalizing Python applications.",
    author = 'Edgewall Software',
    author_email = 'info@edgewall.org',
    license = 'BSD',
    url = 'http://i18n.edgewall.org/',
    download_url = 'http://i18n.edgewall.org/wiki/Download',
    classifiers = [
        'Development Status :: 4 - Beta',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
    packages = find_packages(),
    package_data = {'i18n': ['global.dat', 'i18n/*.dat']},
    include_package_data = True,
    entry_points = """
        [console_scripts]
        pyi18n = i18n.messages.frontend:main

        [distutils.commands]
        compile_catalog = i18n.messages.frontend:compile_catalog
        extract_messages = i18n.messages.frontend:extract_messages
        init_catalog = i18n.messages.frontend:init_catalog
        update_catalog = i18n.messages.frontend:update_catalog

        [distutils.setup_keywords]
        message_extractors = i18n.messages.frontend:check_message_extractors

        [i18n.checkers]
        num_plurals = i18n.messages.checkers:num_plurals
        python_format = i18n.messages.checkers:python_format

        [i18n.extractors]
        ignore = i18n.messages.extract:extract_nothing
        python = i18n.messages.extract:extract_python
        javascript = i18n.messages.extract:extract_javascript
        """
)
