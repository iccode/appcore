import json
import logging
from appcore import settings as s

log = logging.getLogger(__name__)

allow_public = False


def exposed(template=None, methods=None, to=None):
    if not to: to = []

    def wrap(f):
        def wrapped_f(*args, **kw):
            import cherrypy as cp

            req = cp.request
            res = cp.response

            if methods is not None and req.method.lower() not in methods:
                res.headers['Allow'] = ", ".join(methods)
                raise cp.HTTPError(405)

            if len(to) == 0 and not allow_public:
                perm = s.auth_provider(to)
                if not perm: raise cp.HTTPRedirect(
                    s.current_ns.login_url(cp.url()))

            try:
                ct = cp.request.headers["Accept"]
                is_ajax = ct.find("application/json") > -1
                if is_ajax: res.headers["Content-Type"] = "application/json"
            except:
                is_ajax = False

            result = f(*args, **kw)

            if isinstance(result, basestring):
                return result

            if s.has('result_processor'):
                result = s.result_processor(result)

            if is_ajax:
                return json.dumps(result)

            elif template is not None:
                return s.template_provider(template, **result)

            elif not is_ajax and not template:
                return json.dumps(result)

        wrapped_f.exposed = True
        return wrapped_f

    wrap.exposed = True
    return wrap
