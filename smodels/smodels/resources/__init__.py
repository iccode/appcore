from collections import namedtuple
import traceback
import inflect

inflector = inflect.engine()


class ResourceNotFoundError(Exception):
    """
    Thrown when a resource is not found
    """
    pass


class UnsupportedActionError(Exception):
    """
    Thrown when an unsupported action is performed in a resource
    """
    pass


_collection = namedtuple("collection", ['of'])


def _collection_of(t):
    if callable(t) and not isinstance(t, type):
        t = t()

    assert isinstance(t, type)
    if not hasattr(t, '__resource__'):
        raise Exception(
            "Cannot create collection. Supplied type is not a resource")

    return _collection(of=t.__resource__['name'])


def collection_of(resource):
    """ Decorates a function as a collection accessor. As an argument
        it takes the name of a resource, THE PLURAL name.
    """

    def dec(func):

        func._collection = _collection(resource)

        def make_appender(appender_fun):
            func.append = appender_fun
            return appender_fun

        func.appender = make_appender
        return func

    return dec


def action():
    """
    Decorates a function as an action accessor
    """

    def dec(func):
        func._action = ('action',)
        return func

    return dec


import resource_registry as registry


class ResourceMeta(type):
    """ Resource classes metaclass. 
        The main responsibility of ResourceMeta is to register resources
        as they are defined in the source.

    """
    def __init__(cls, name, bases, dct):
        if hasattr(cls, "__resource__"):
            try:
                registry.register(cls)
            except:
                import warnings

                warnings.warn("Unable to register class %s as resource" % cls)
                traceback.print_exc()

        super(ResourceMeta, cls).__init__(name, bases, dct)


no_internal = lambda x: x[0:2] != '__' and x[:2] != '__'
no_special = lambda x: x[0] != '_' and x[-1] != '_'


class ResourceBase(object):
    """ A VERY abstract resource base class.
        Usually most of these methods are already provided by SAEntity, 
        but if we want to make a non-db-entity resource, we can inherit
        that class.
    
    """
    __metaclass__ = ResourceMeta

    def delete(self):
        raise NotImplementedError

    @classmethod
    def load(cls, key):
        raise NotImplementedError

    @classmethod
    def all(all):
        raise NotImplementedError

    def bind(self, **kw):
        for k, v in kw.items():
            if hasattr(self, k):
                setattr(self, k, v)

        return self

    def representation(self, view='default'):
        return {
            a: getattr(self, a) for a in
            filter(lambda x: no_internal(x) and no_special(x), dir(self))
            if not callable(getattr(self, a))
        }
