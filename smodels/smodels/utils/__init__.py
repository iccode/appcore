from . import conv

__author__ = 'oglu'

class ParamsDict(dict):
    """
    A dictionary subclass mainly used for processing keyword args.
    It has two kind of methods
        * `sf_<type_name>` which return a default value if key is not found
        * `<type_name>` which throw a key error if key is not found
    """
    def __init__(self,dct=None,**kw):
        import pprint
        pprint.pprint(dct)
        if dct:
            super(ParamsDict,self).__init__(**dct)
        else:
            super(ParamsDict,self).__init__(**kw)


    def __getattr__(self, name):
        if "_" in name:
            method, type = name.split('_',1)
        else:
            method = 'get'
            type = name

        callable = getattr(self, method)
        type_callable = getattr(conv, "p_"+type)

        def fn(key, default=None):
            try:
                return type_callable(callable(key), default)
            except:
                return default

        return fn


