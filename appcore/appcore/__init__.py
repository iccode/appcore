import sys
from os.path import dirname, join

__LIB_ROOT__ = join(dirname(__file__), "lib")
sys.path.append(__LIB_ROOT__)


from appcore.templates import mako_provider
from ._settings import _Settings

settings = _Settings()
settings.CP_CONFIG = {
    'tools.encode.on': True,
    'tools.encode.encoding': 'utf-8',

    'tools.decode.on': True,
    'tools.decode.encoding': 'utf-8',

    'tools.sessions.on': True,
    'tools.sessions.storage_type': "redis",
    'tools.sessions.clean_thread': True,
    'tools.sessions.timeout': 100,
}

def auth_provider(to):
    import cherrypy as cp
    return cp.session.get('user') if hasattr(cp, 'session') and cp.session is not None else None

settings.auth_provider = auth_provider

class AppError(BaseException):
    pass


def set_locale(locale):
    pass


def get_locale():
    pass

from .web import get_namespace, set_namespace, make_app
