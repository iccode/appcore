import cherrypy as cp
from cherrypy._cperror import HTTPRedirect

from appcore import di, settings, get_namespace
from appcore.auth.dbauth import DbAuthController
from ..auth.gauth import GAuthController
from ..base import CPController


class NamespaceAuthController(CPController):
    def __init__(self):
        self.__google = di.make_instance(GAuthController)
        self.__db = di.make_instance(DbAuthController)

        settings.login_url = self.get_login_url

    def get_login_url(self, r):
        mode = settings.current_ns.auth_mode

        if mode == 'google':
            return self.__google.create_login_url(r)
        if mode == 'db':
            return self.__db.create_login_url(r)

    def login(self, username=None, password=None, r='/', **kw):
        mode = settings.current_ns.auth_mode
        if mode == 'db':
            return self.__db.login(username, password, r)
        if mode == 'google':
            raise HTTPRedirect(self.__google.create_login_url(r))

        assert False  # shouldn't be here
    login.exposed = True

    def logout(self):
        mode = settings.current_ns.auth_mode
        if mode == 'google':
            return self.__google.logout()
        if mode == 'db':
            return self.__db.logout()
        cp.session.clear()
    logout.exposed=True

    def cb(self, **kw):
        mode = settings.current_ns.auth_mode
        if mode == 'google':
            return self.__google.cb(**kw)

    cb.exposed=True
