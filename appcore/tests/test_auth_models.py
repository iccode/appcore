import appcore.auth.models as m
from smodels.session import DbSession
from fixtures import engine, setup_module, teardown_module


def test_insert(engine):
    c = m.AppUser.create(
        "ns",
        "user",
        "pass"
    )
    with DbSession(engine=engine) as sess:
        sess.add(c)
        sess.commit()

    with DbSession(engine=engine):
        logged = m.AppUser.login("ns", "user", "pass")

    assert logged is not None


def test_superadmin(engine):
    c = m.AppUser.create(
        "ns",
        "user",
        "pass"
    )
    c.groups = ["superadmin"]

    with DbSession(engine=engine) as sess:
        sess.add(c)
        sess.commit()

    with DbSession(engine=engine):
        logged = m.AppUser.login("ns", "user", "pass")

    assert logged.can("res", "test")


def test_groups(engine):
    with DbSession(engine=engine) as sess:
        group = m.AppGroup(namespace="ns", code='grp')
        group['res'] = {'edit', 'delete'}

        sess.add(group)

        u = m.AppUser.create('ns', 'user', 'pass')
        u.groups = ['grp']
        sess.add(u)

        sess.commit()

    with DbSession(engine=engine) as sess:
        logged = m.AppUser.login('ns', 'user', 'pass')

        assert logged.can('res', 'edit')
        assert logged.can('res', 'delete')
        assert not logged.can('res', 'create')
