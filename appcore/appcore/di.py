import importlib
import inspect
import os
from types import FunctionType, MethodType

__REGISTRY = {}


class DIError(Exception):
    pass


def find_class(class_):
    """
    Loads a class by it's path
    :param class_:
    :type class_: str, unicode
    :return: The class type
    :rtype:  type
    """
    assert isinstance(class_, basestring)
    toks = class_.split('.')
    className = toks[-1]
    module = ".".join(toks[:-1])
    if not module:
        return None

    module = importlib.import_module(module)
    return getattr(module, className)


def register(name, value, replace=False):
    if not isinstance(name, (basestring, type)):
        raise ValueError("A string or type is expected")

    if isinstance(name, type):
        name = "__type::" + str(type)

    if name in __REGISTRY and not replace:
        raise DIError("Dependency already registered. Use replace to"
                      "change it")

    __REGISTRY[name] = value


def make_instance(class_, parent_=None):
    assert isinstance(class_, type)

    if not isinstance(class_.__init__, (FunctionType, MethodType)):
        return class_()

    argspec = inspect.getargspec(class_.__init__)
    args = argspec.args[1:]
    vals = {}
    for a in args:
        if a == parent_: raise DIError("Cyclical dependency on %s" % a)

        vals[a] = get(a, class_)

    return class_(**vals)


def call(fun):
    assert isinstance(fun, FunctionType)

    argspec = inspect.getargspec(fun)

    vals = {}
    defs = argspec.defaults or []
    for i, a in enumerate(argspec.args):
        if a == "self": continue
        vals[a] = get(a, argspec.defaults[i] if i < len(defs) else None)

    return fun(**vals)


def get(name, parent_=None, default=None):
    if name in __REGISTRY:
        value = __REGISTRY[name]
        if isinstance(value, FunctionType):
            return call(value)

        return value

    if isinstance(name, type):
        val = name
    elif isinstance(name, basestring):
        val = find_class(name)
    else:
        val = None

    if val is None:
        val = os.environ.get(name.upper(), default)

    if isinstance(val, type): return make_instance(val, parent_)
    if isinstance(val, FunctionType): return call(val)

    return val


def resolve(what):
    if isinstance(what, basestring):
        return get(what)

    if isinstance(what, type):
        name = "__type::" + str(type)
        value = get(name)

        if isinstance(value, type):
            return make_instance(value)
