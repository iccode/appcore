from appcore import set_namespace
from appcore._settings import _Settings


def test_settings():
    s = _Settings()
    s.hello = 'ome'
    s.ns('test').hello = 'test_ns_hello'

    assert s.hello == 'ome'
    assert s.current_ns.ns == 'default'
    assert s.current_ns.hello == 'ome'

def test_settings_fallback():
    s = _Settings()
    s.hello = 'ome'
    s.ns('testn.gr').other_val = 'test_ns_hello'

    assert s.ns('testn.gr').hello == 'ome'

    set_namespace('testn.gr')

    assert s.current_ns.other_val == 'test_ns_hello'
    assert s.current_ns.hello == 'ome'

