__doc__ = """
Template Related logic
-------------------------

There are two scenarios that we try to accommodate with this mechanism.

1) Normal http request (those coming from a browser)
2) XmlHttpRequest requests (those coming from ajax calls)

In the first scenario, everything is simple and we just render the
template and send it.

On the second one, we have to strip the base template inheritance from
the mako template and return a bare one.

As a side note, some templates may be written in a mustache/handlebars type.
These templates can be reused by the application to render content client-side,
without doing the extra request to fetch the resource (or maybe we only have
a client-side resource). To render them server-side we transform the stache
templates to mako templates[1].

On a general note, the template folder is still in use, as we have tmeplates
that do not need to be rendered client-side or requested chromeless.

[1]: We do that via a mako preprocessor, that gets the stache and spits mako

* NOTE: Chromeless is a template rendering without the actual application chrome
(menus, headers, footers, etc)

* NOTE2: Whenever you see "stache" we mean mustache compatible templates
"""
import re
import logging
from StringIO import StringIO

from .lib.mako.template import Template
from .lib.mako.lookup import TemplateLookup

import cherrypy as cp

log = logging.getLogger(__name__)

___STACHE_RE = re.compile(r"""
(?P<whitespace>[\ \t]*)
     %(otag)s \s*
     (?:
       (?P<change>=) \s* (?P<delims>.+?)   \s* = |
       (?P<raw>{)    \s* (?P<raw_name>.+?) \s* } |
       (?P<tag>[%(tag_types)s]?)  \s* (?P<tag_key>[\s\S]+?)
     )
\s* %(ctag)s
""" % {
    'tag_types': "!>&/#^",
    'otag': re.escape('{{'),
    'ctag': re.escape('}}')
}, re.VERBOSE
)

__MAKO_HEADER = """<%!
from mako.runtime import Context
def pick(obj,attr):
  if isinstance(obj,dict): return obj[attr]
  if isinstance(obj,Context): return obj[attr]

  return getattr(obj,attr)
%>"""


def stache_to_mako(tmpl):
    """
    Tranforms a  stache template to a mako one.
    The transformation is mostly moving from stache semantics ({{ and stuff))
    to ${xx} stuff that is understood by mako.

    Right now we support simple variable substitution and looping (via {{# list
    tag)

    :param tmpl:
    :type tmpl:
    :return:
    :rtype:
    """
    io = StringIO()
    io.write(__MAKO_HEADER)

    idx = 0
    tags = []

    #we get the regexes that correspond to stache tags
    while True:
        m = ___STACHE_RE.search(tmpl, idx)
        if m is None: break
        tags.append(m)
        idx = m.end()

    idx = 0
    otag = None
    tags.reverse()

    SPECIAL = frozenset(('else', 'include'))

    #we need to transform expressions from <obj>.<obj>
    #to actual python things. That is we call pick
    #function in the header that does the magic of choosing
    #how to access data
    #
    # We need the pick function cause in stache templates
    # there is no different syntax for object access and
    # dictionary access.
    def _expr(key):
        if key[0:3] == '../': key = key[3:]
        if "." in key:
            toks = key.split('.')
            obj = 'context'
            toks.reverse()
            while len(toks):
                o = toks.pop()
                obj = 'pick(%s,"%s")' % (obj, o)
            key = obj

        return key

    while len(tags):
        t = tags.pop()
        dt = t.groupdict()
        io.write(tmpl[idx:t.start()])
        tag = dt['tag']

        try:
            key, expr = dt['tag_key'].split(' ')
        except:
            key, expr = dt['tag_key'], ''

        if tag == '' and not otag and key not in SPECIAL:
            io.write("${%s}" % _expr(key))
        if tag == '' and key in SPECIAL:
            if key == 'else':
                io.write('\n% else:')
            elif key == 'include':
                io.write("<%%include file='%s'/>\n" % expr)
        elif tag == '' and otag == 'l':
            if key[0:3] == '../':
                key = key[3:]
                io.write('${%s}' % _expr(key))
            else:
                io.write("${pick(i,'%s')}" % key)
        elif tag == '#':
            if key in ('list', 'each'):
                otag = 'l'
                io.write("%%for i in %s:\n" % _expr(expr))
            elif key == 'if':
                io.write('\n%% if %s:' % _expr(expr))
        elif tag == '/':
            if otag == 'l':
                io.write("\n%endfor")
                otag = None
            if key.startswith("if"):
                io.write("\n% endif")

        idx = t.end()

    io.write(tmpl[idx:])

    return io.getvalue()


_tmplLookup = None


def get_chromeless(template_name):
    """
    Returns a chromeless template to be used for rendering.

    The solution right now is to parse the source of a template
    and pick the second line (which includes the main template)

    :param template_name:
    :type template_name:
    :return:
    :rtype:
    """
    tmpl = _tmplLookup.get_template(template_name)
    src = tmpl.source
    lines = src.strip().split('\n')
    if len(lines) == 2:
        return Template(lines[1], lookup=_tmplLookup,
                        filename=template_name + "_chromeless")

    assert False  # we shouldn't be here


def pjax_render_template(name, **kw):
    """
    Returns a template to be rendered.
    If a request is made by X-Requested-With we try to get a chromeless
    version of the template by using the get_chromeless function with
    a template name.
    :param name: The template name (a path relative to template dirs)
    :type name: string
    :param kw: The keyword args to use as binding context
    :return: The rendered str
    :rtype: string
    """
    try:
        heads = cp.request.headers
        if "X-Requested-With" in heads and heads[
                'X-Requested-With'] == 'XMLHttpRequest':

            chromeless = get_chromeless(name)
            return chromeless.render(**kw)
    except:
        log.error("Unable to render chromeless %s" % name, exc_info=True)
        pass  # we failed for whatever reason

    return render_unicode_template(name, **kw)


def mako_provider(dirs):
    global _tmplLookup
    _tmplLookup = TemplateLookup(directories=dirs,
                                 output_encoding='utf-8',
                                 input_encoding='utf-8',
                                 default_filters=['decode.utf8'],
                                 preprocessor=stache_to_mako)

    return render_unicode_template


def render_unicode_template(template, **context):

    from . import settings as s
    if s.has('global_rendering_context'):
        gctx = s.global_rendering_context
        context.update(gctx if isinstance(gctx, dict) else gctx())
    return _tmplLookup.get_template(template).render_unicode(**context)
