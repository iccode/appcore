from argparse import ArgumentParser
from appcore.auth.models import AppUser
from smodels.session import DbSession

parser = ArgumentParser(description='Adds an administrator')
parser.add_argument("namespace", type=str, help='Namespace to add the admin to')
parser.add_argument("username", type=str, help='Username of the admin')
parser.add_argument("password", type=str, help='Password of the admin')


def command(**kw):
    with DbSession() as sess:
        u = AppUser.create(**kw)
        u.groups = ['admin']
        sess.add(u)
        sess.commit()

command.parser = parser

